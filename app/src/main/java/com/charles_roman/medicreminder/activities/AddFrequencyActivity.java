package com.charles_roman.medicreminder.activities;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import com.charles_roman.medicreminder.R;
import com.charles_roman.medicreminder.db.entity.Frequency;
import com.charles_roman.medicreminder.db.entity.Medication;

import java.util.Date;


public class AddFrequencyActivity extends AppCompatActivity implements BaseActivity
{
    TextView _inputHour;

    TextView labelHour;

    Frequency  currentFrequency;
    Medication currentMedication;

    Button saveFrequencyButton;

    static final int DEBUT_DATE = 1;
    static final int END_DATE   = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_frequency);

        initUI();
        initData();
    }

    @Override
    public void initData()
    {
        Long frequencyId = getIntent().getLongExtra("frequency_id", 0);
        currentFrequency = (frequencyId == 0) ? new Frequency() : Frequency.findById(Frequency.class, frequencyId);

        // SET ASSOCIATED MEDICATION
        Long medicationId = getIntent().getLongExtra("medication_id", 0);
        currentMedication = Medication.findById(Medication.class, medicationId);

        dispatchData();
    }

    public void dispatchData()
    {
        if (currentFrequency.hour != 0 && currentFrequency.minute != 0) {
            String textHour = currentFrequency.hour + ":" + currentFrequency.minute;
            _inputHour.setText(textHour);
        }
    }

    @Override
    public void initUI()
    {
        // ELEMENTS GATHERING
        _inputHour = (TextView) findViewById(R.id.input_hour);
        labelHour = (TextView) findViewById(R.id.label_hour);
        saveFrequencyButton = (Button) findViewById(R.id.button_save_frequency);

        // HOUR SELECTOR
        _inputHour.setOnClickListener(new HourSelectorListener());
        labelHour.setOnClickListener(new HourSelectorListener());

        // FIELDS
        saveFrequencyButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                saveFrequency();
                finish();
            }
        });

    }

    public void saveFrequency()
    {
        currentFrequency.medication = currentMedication;
        currentFrequency.save();
    }

    /**
     * Hour On Lick Listener
     */
    public class HourSelectorListener implements View.OnClickListener
    {

        @Override
        public void onClick(View v)
        {
            HourSelector hourSelector = new HourSelector(v.getContext(), currentFrequency.hour,
                                                         currentFrequency.minute);
        }
    }

    /**
     * Hour Dialog Box
     */
    public class HourSelector extends AlertDialog.Builder
    {
        int hour;
        int minute;

        AlertDialog boxContext;
        TimePicker  timePicker;

        HourSelector(@NonNull Context context, int hour, int minute)
        {
            super(context);
            this.hour = hour;
            this.minute = minute;
            init();
        }

        void init()
        {
            boxContext = this.setView(R.layout.hour_selector).show();

            Button buttonHourSelector = (Button) boxContext.findViewById(R.id.button_hour_selector);
            timePicker = (TimePicker) boxContext.findViewById(R.id.timePicker);

            if (timePicker == null || buttonHourSelector == null) {
                return;
            }

            timePicker.setHour(this.hour);
            timePicker.setMinute(this.minute);

            buttonHourSelector.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View view)
                {
                    hour = timePicker.getHour();
                    minute = timePicker.getMinute();

                    String textHour = hour + ":" + minute;

                    currentFrequency.hour = hour;
                    currentFrequency.minute = minute;

                    _inputHour.setText(textHour);
                    boxContext.hide();

                }

            });
        }
    }
}

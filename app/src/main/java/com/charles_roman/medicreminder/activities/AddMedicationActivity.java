package com.charles_roman.medicreminder.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.charles_roman.medicreminder.R;
import com.charles_roman.medicreminder.adapter.ListFrequencyAdapter;
import com.charles_roman.medicreminder.adapter.ListMedicationAdapter;
import com.charles_roman.medicreminder.db.entity.Medication;

public class AddMedicationActivity extends AppCompatActivity implements BaseActivity
{
    EditText _inputName;
    EditText _inputDescription;
    EditText _inputDuration;

    Button addFrequencyButton;
    Button saveMedicationButton;

    Medication currentMedication;

    RecyclerView _listFrequency;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_medication);

        //initUI();
        //initData();
    }

    protected void onStart()
    {
        super.onStart();
        initData();
    }

    @Override
    public void initData()
    {
        Long medicationId = getIntent().getLongExtra("medication_id", 0);
        currentMedication = (medicationId == 0) ? new Medication() : Medication.findById(Medication.class,
                                                                                         medicationId);
        _listFrequency.setAdapter(new ListFrequencyAdapter());

        dispatchData();
    }

    public void dispatchData()
    {
        _inputName.setText(currentMedication.name);
        _inputDescription.setText(currentMedication.description);
        _inputDuration.setText(currentMedication.duration);
    }

    @Override
    public void initUI()
    {
        _inputName = (EditText) findViewById(R.id.input_name);
        _inputDescription = (EditText) findViewById(R.id.input_description);
        _inputDuration = (EditText) findViewById(R.id.input_duration);

        _listFrequency = (RecyclerView) findViewById(R.id.list_frequency);
        _listFrequency.setLayoutManager(new LinearLayoutManager(this));

        addFrequencyButton = (Button) findViewById(R.id.button_add_frequency);
        saveMedicationButton = (Button) findViewById(R.id.button_save_medication);

        addFrequencyButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                saveMedication();
                Intent intent = new Intent(AddMedicationActivity.this, AddFrequencyActivity.class);
                intent.putExtra("medication_id", currentMedication.getId());
                startActivity(intent);
            }
        });

        saveMedicationButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                saveMedication();
                finish();
            }
        });
    }

    public void saveMedication()
    {

        currentMedication.name = _inputName.getText().toString();
        currentMedication.description = _inputDescription.getText().toString();
        currentMedication.duration = Integer.getInteger(_inputDescription.getText().toString());

        currentMedication.save();

        dispatchData();

    }
}

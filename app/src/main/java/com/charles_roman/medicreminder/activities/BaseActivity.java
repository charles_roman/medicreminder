package com.charles_roman.medicreminder.activities;

public interface BaseActivity
{
    void initData();

    void initUI();
}

package com.charles_roman.medicreminder.activities;

import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import com.charles_roman.medicreminder.R;
import com.charles_roman.medicreminder.adapter.ListMedicationAdapter;

import java.util.List;

public class MainActivity extends AppCompatActivity implements BaseActivity
{
    Toolbar              _toolbar;
    FloatingActionButton _addButton;
    RecyclerView         _listMedication;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();
        initData();

    }

    protected void onStart()
    {
        super.onStart();
        initData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void initData()
    {
        _listMedication.setAdapter(new ListMedicationAdapter());
    }

    @Override
    public void initUI()
    {
        _toolbar = (Toolbar) findViewById(R.id.toolbar);
        _addButton = (FloatingActionButton) findViewById(R.id.fab);

        _listMedication = (RecyclerView) findViewById(R.id.list_medication);
        _listMedication.setLayoutManager(new LinearLayoutManager(this));

        setSupportActionBar(_toolbar);

        _addButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                startActivity(new Intent(MainActivity.this, AddMedicationActivity.class));
            }
        });
    }
}

package com.charles_roman.medicreminder.db.entity;

import com.orm.SugarRecord;

import java.util.Date;

public class Frequency extends SugarRecord
{
    public int hour;
    public int minute;

    public Medication medication;

    public Frequency()
    {
    }
}

package com.charles_roman.medicreminder.db.entity;

import com.orm.SugarRecord;

import java.util.Date;
import java.util.List;

public class Medication extends SugarRecord
{
    public String name;
    public String description;
    public int duration;
    public int day_left;

    public Medication()
    {
    }

    List<Frequency> getFrequencies()
    {
        return Frequency.find(Frequency.class, "medication = ?", String.valueOf(getId()));
    }
}

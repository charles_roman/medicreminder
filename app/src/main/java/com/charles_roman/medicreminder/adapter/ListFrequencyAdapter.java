package com.charles_roman.medicreminder.adapter;


import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.charles_roman.medicreminder.R;
import com.charles_roman.medicreminder.activities.AddFrequencyActivity;
import com.charles_roman.medicreminder.db.entity.Frequency;

import java.util.List;

public class ListFrequencyAdapter extends RecyclerView.Adapter<ListFrequencyAdapter.MyViewHolder>
{
    private final List<Frequency> frequencies;
    private       Context         parentContext;

    public ListFrequencyAdapter()
    {
        frequencies = Frequency.listAll(Frequency.class);
    }

    @Override
    public ListFrequencyAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        parentContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(parentContext);
        View           view     = inflater.inflate(R.layout.medication_cell, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ListFrequencyAdapter.MyViewHolder holder, int position)
    {
        Frequency frequency = frequencies.get(position);
        holder.display(frequency);
    }

    @Override
    public int getItemCount()
    {
        return frequencies.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        private final TextView label;

        private Frequency currentFrequency;

        MyViewHolder(View itemView)
        {
            super(itemView);
            label = (TextView) itemView.findViewById(R.id.label_frequency);
            itemView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent(parentContext, AddFrequencyActivity.class);
                    intent.putExtra("frequency_id", currentFrequency.getId());
                    parentContext.startActivity(intent);
                }
            });
        }

        void display(Frequency frequency)
        {
            currentFrequency = frequency;
            String labelText = "test";
            label.setText(labelText);
        }
    }
}

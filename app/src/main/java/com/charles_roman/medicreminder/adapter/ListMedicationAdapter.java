package com.charles_roman.medicreminder.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.charles_roman.medicreminder.R;
import com.charles_roman.medicreminder.activities.AddMedicationActivity;
import com.charles_roman.medicreminder.db.entity.Medication;

import java.util.List;

public class ListMedicationAdapter extends RecyclerView.Adapter<ListMedicationAdapter.MyViewHolder>
{
    private final List<Medication> medications;
    private       Context          parentContext;

    public ListMedicationAdapter()
    {
        medications = Medication.listAll(Medication.class);
    }

    @Override
    public ListMedicationAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        parentContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(parentContext);
        View           view     = inflater.inflate(R.layout.medication_cell, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ListMedicationAdapter.MyViewHolder holder, int position)
    {
        Medication medication = medications.get(position);
        holder.display(medication);
    }

    @Override
    public int getItemCount()
    {
        return medications.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder
    {
        private final TextView name;
        private final TextView description;

        private Medication currentMedication;

        MyViewHolder(View itemView)
        {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.medication_name);
            description = (TextView) itemView.findViewById(R.id.medication_description);
            itemView.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {
                    Intent intent = new Intent(parentContext, AddMedicationActivity.class);
                    intent.putExtra("medication_id", currentMedication.getId());
                    parentContext.startActivity(intent);
                }
            });
        }

        void display(Medication medication)
        {
            currentMedication = medication;
            name.setText(medication.name);
            description.setText(medication.description);
        }
    }
}
